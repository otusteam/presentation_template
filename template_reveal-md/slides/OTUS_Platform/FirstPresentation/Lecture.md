
![tran](/assets/otus_first_pages.png)<!-- .element: width="100%" -->

---

![tran](/assets/otus_sound_check.png)<!-- .element: width="100%" -->

---

# НЕ ЗАБЫТЬ ВКЛЮЧИТЬ ЗАПИСЬ!!!

---

# Название вашей презентации

---

## Первая тема

* Элемент списка №1
* Элемент списка №2
* Элемент списка №3

---

## Вторая тема

* Элемент списка №1
* Элемент списка №2
* Элемент списка №3

---

## Пример кода

```bash
docker ps
container_id=<container_id>
pid=$(docker inspect -f '{{.State.Pid}}' ${container_id})
ln -sfT /proc/$pid/ns/net /var/run/netns/pmm
ip netns exec pmm ss -tlpn
```    
вход в неймспейс
```bash
nsenter --net=/proc/$pid/ns/net 
ss -tlpn
```

## Пример ссылок
* [microservices docker-compose.yml](https://github.com/microservices-demo/microservices-demo/blob/master/deploy/docker-compose/docker-compose.yml)
* [CIS bets practise](https://www.cisecurity.org/cybersecurity-best-practices/)
    * controls
    * benchmarks
* [lynis](https://cisofy.com/lynis/)
* [docker-bench-security](https://github.com/docker/docker-bench-security)

---

![tran](/assets/otus_your_questions.png)<!-- .element: width="100%" --> 

---

![tran](/assets/otus_feedback.png)<!-- .element: width="100%" -->

